% lambda is the regularization factor: lambda=0: no regularization. 
% Large lambda ==> large regularization effect

clear all;
close all;

tic;

order = 5;     %This is the number of features
data = importdata('data.txt');
X = ones(size(data,1),1);
for k=1:order
    X = [X data(:,1).^k];
end
y = data(:,2);
method = 'linear';

%% feature normalization!
[X(:,2:end) mu sigma] = featureNormalize(X(:,2:end));
% end

m = size(data,1);
lambda = 0;      %regularization factor
options = optimset('GradObj', 'on', 'MaxIter', 5000,'MaxFunEval',5000,'TolFun',1e-10);

switch method
    case 'linear'
%% For linear regression:
        init_theta = zeros(order+1,1);

        %fitting function h(x) = theta_0+theta_1*x+theta_2*x^2 + ...
        [theta,J,exit_flag] = fminunc(@(t) (costfunc_reg_linear(t,X,y,lambda)),init_theta,options);
        fprintf('Final cost function is: %f\n',J);

        % Begin plotting:
        figure (1)
        scatter(data(:,1),data(:,2))
        hold on
        h=0.01;
        x_grid = transpose(0:h:7);
        x_plot = ones(length(x_grid),1);
        for k=1:order
            x_plot = [x_plot x_grid.^k];
        end
        x_plot = bsxfun(@minus,x_plot,[0 mu]);
        x_plot = bsxfun(@rdivide,x_plot,[1 sigma]);

        y_grid = x_plot*theta;
        plot(x_grid,y_grid);
        title('Zeroth derivative','fontsize',22)
        y_1 = diff(y_grid)/h;
        y_2 = diff(y_1)/h;
        y_3 = diff(y_2)/h;
        
        figure (2)
        plot(x_grid(1:length(y_1)),y_1);
        hold on
        plot(x_grid,cos(x_grid),'r')
        hold on
        y_1_direct = diff(y)/h;
        plot(data(1:length(y_1_direct),1),y_1_direct,'g')
        title('First derivative','fontsize',22)
        
        figure (3)
        plot(x_grid(1:length(y_2)),y_2);
        hold on
        plot(x_grid,-sin(x_grid),'r')
        title('Second derivative','fontsize',22)

        figure (4)
        plot(x_grid(1:length(y_3)),y_3);
        hold on
        plot(x_grid,-cos(x_grid),'r')
        title('Third derivative','fontsize',22)



    case 'exponential'
%% For exponential regression with  h(x) = exp(theta_0+theta_1*x+theta_2*x^2 + ...) + a0
% a0 is a constant offset _*at the end of theta array*_ .
        init_theta = zeros(order+2,1);

        %fitting function h(x) = exp(theta_0+theta_1*x+t    heta_2*x^2 + ...)
        [theta,J,exit_flag] = fminunc(@(t) (costfunc_reg_exponential(t,X,y,lambda)),init_theta,options);
        fprintf('Final cost function is: %f\n',J);

        %begin plotting:
        figure (1)
        plot(data(:,1),data(:,2),'o')
        hold on
        h = 1e-2;  %step size
        x_grid = transpose(0:h:23.4);
        x_plot = ones(length(x_grid),1);
        for k=1:order
            x_plot = [x_plot x_grid.^k];
        end
        x_plot = bsxfun(@minus,x_plot,[0 mu]);
        x_plot = bsxfun(@rdivide,x_plot,[1 sigma]);

        y_grid = exp(x_plot*theta(1:end-1))+theta(end);
        plot(x_grid,y_grid,'r');
        title('Zeroth derivative','fontsize',22)
        y_1 = diff(y_grid)/h;
        y_2 = diff(y_1)/h;
        y_3 = diff(y_2)/h;
        
        figure (2)
        plot(x_grid(1:length(y_1)),y_1);
        title('First derivative','fontsize',22)
        
        figure (3)
        plot(x_grid(1:length(y_2)),y_2);
        title('Second derivative','fontsize',22)

        figure (4)
        plot(x_grid(1:length(y_3)),y_3);
        title('Third derivative','fontsize',22)
        
        theta

end

toc;