function [J, grad] = costfunc_reg_linear(theta,X,y,lambda)

%costfunc_reg Compute cost and gradient for logistic regression with regularization
%   J = COSTFUNCTIONREG(theta, X, y, lambda) computes the cost of using
%   theta as the parameter for regularized logistic regression and the
%   gradient of the cost w.r.t. to the parameters. 

% Initialize some useful values
m = length(y); % number of training examples
%grad = zeros(size(theta));

%% for linear regression!
J = 0.5/m*(X*theta-y)'*(X*theta-y) + lambda/(2*m)*(theta'*theta-theta(1)^2);
grad = 1/m*X'*(X*theta-y);
grad(2:end) = grad(2:end)+lambda/m*theta(2:end);

end