% This code does linear fitting of log-transformed data of life time measurements
tic;
close all;
clear all;
% =========Set how many exponentials are used in the regression model ==========
order=1;                                                                      %=
% ==============================================================================

data_struct=importdata('HMMonGlass_650nm.fit');
data=data_struct.data;
newdata=data(:,1:2);

low=64;
high=230;

X=newdata(low:high,1);
y=newdata(low:high,2);

figure (1)
plot(newdata(:,1),newdata(:,2),'.');
hold on

m=length(y);
options = optimset('GradObj', 'on', 'MaxIter', 500000,'MaxFunEval',500000,'TolFun',1e-10);
init_theta=zeros(order*2+1,1);      % Total number of parameters. Each exponential is associated with a magnitude and tau
[theta,J,exit_flag] = fminunc(@(t) costfunc_multiexpo(t,X,y),init_theta,options);
fprintf('Final cost function is: %f\n',J);

time_grid=linspace(newdata(60,1),newdata(end,1),1000);
y_hat_draw = theta(1,1);
for ip=1:order
    y_hat_draw = y_hat_draw+theta(ip*2,1)*exp(-theta(ip*2+1)*time_grid);
end

plot(time_grid,y_hat_draw, 'linewidth',2);
title('Transience data and fitting', 'fontsize',24)
xlabel('Time (ns)','fontsize',24)
ylabel('Counts','fontsize',24)
set(gca,'fontsize',24)
hold off

figure (2)
semilogy(newdata(:,1),newdata(:,2),'.');
hold on
semilogy(time_grid,y_hat_draw, 'linewidth',2);
title('Log transience data and fitting', 'fontsize',24)
xlabel('Time (ns)','fontsize',24)
ylabel('Counts','fontsize',24)
set(gca,'fontsize',24)
hold off

y_hat_pred = theta(1,1);
for ip=1:order
    y_hat_pred = y_hat_pred+theta(ip*2,1)*exp(-theta(ip*2+1)*X);
end

residual=y-y_hat_pred;     % regression residuals
SSresid=sum(residual.^2);       % calculate residual sum of squares
SStotal=length(X)*var(y);
rsq = 1 - SSresid/SStotal;
fprintf('===============================================================================');
fprintf('\nThe fitting r-squared is: %f\n',rsq);
fprintf('There are in total %d time constants\n',order);
for ip=1:order
    fprintf('The %d th time constant is %f ns\n',ip,1/theta(ip*2+1));
end
fprintf('===============================================================================');


figure(3)
plot(X,residual,'.','markers',10);
title('Residual plot','fontsize',24)
xlabel('Time (ns)','fontsize',24)
ylabel('Residual','fontsize',24)
set(gca,'fontsize',24)

toc;