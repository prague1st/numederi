function [J, grad] = costfunc_multiexpo(theta,X,y)

%costfunc_reg Compute cost and gradient for logistic regression with regularization
%   J = COSTFUNCTIONREG(theta, X, y, lambda) computes the cost of using
%   theta as the parameter for regularized logistic regression and the
%   gradient of the cost w.r.t. to the parameters. 

% Initialize some useful values
m = length(y);          % number of training examples
p = length(theta);      % number of parameters
%grad = zeros(size(theta));

%% For multiple exponential regression:
count_k=3:2:p;
count_I=2:2:p;
exponents = exp(-X*(theta(count_k,:))');      % m*order matrix
y_tx=(y-theta(1,1)-exponents*theta(count_I,:));

J = 0.5/m*(y_tx')*y_tx;
grad=-1/m*sum(y_tx);
for count=1:length(count_I)
    grad1=-1/m*sum(y_tx.*exponents(:,count));
    grad2=1/m*sum(theta(count_I(count))*X.*y_tx.*exponents(:,count));
    grad=[grad;grad1;grad2];
end
