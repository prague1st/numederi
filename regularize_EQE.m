clear all;
close all;

tic;

order = 10;     %This is the number of features
data = importdata('Irppy_EQE.txt');
data(:,1) = data(:,1)*1000;
X = ones(size(data,1),1);
for k=1:order
    X = [X log10(data(:,1)).^k];
end
y0 = data(:,2);     %EQE0
y1 = data(:,3);     %EQE1
y2 = log10(data(:,4));     %L0
method = 'linear';

%% feature normalization!
[X(:,2:end) mu sigma] = featureNormalize(X(:,2:end));
% end

m = size(data,1);
lambda = 0;      %regularization factor
options = optimset('GradObj', 'on', 'MaxIter', 5000,'MaxFunEval',5000,'TolFun',1e-10);

switch method
    case 'linear'
%% For linear regression:
        init_theta = zeros(order+1,1);

        %fitting function h(x) = theta_0+theta_1*x+theta_2*x^2 + ...
        [theta0,J0,exit_flag] = fminunc(@(t) (costfunc_reg_linear(t,X,y0,lambda)),init_theta,options);
        [theta1,J1,exit_flag] = fminunc(@(t) (costfunc_reg_linear(t,X,y1,lambda)),init_theta,options);
        [theta2,J2,exit_flag] = fminunc(@(t) (costfunc_reg_linear(t,X,y2,lambda)),init_theta,options);
        fprintf('Final cost function is: %f\n',J0);

        % Begin plotting:
        figure (1)
        semilogx(data(:,1),data(:,2),'o')
        hold on
        h = 1e-1;  %step size
        x_grid = transpose(1.5e-1:h:250);
        x_plot = ones(length(x_grid),1);
        for k=1:order
            x_plot = [x_plot log10(x_grid).^k];
        end
        x_plot = bsxfun(@minus,x_plot,[0 mu]);
        x_plot = bsxfun(@rdivide,x_plot,[1 sigma]);
        EQE0_regre = x_plot*theta0;
        semilogx(x_grid,EQE0_regre);
        theta0
        
        figure (2)
        semilogx(data(:,1),y1,'o')
        hold on
        EQE1_regre = x_plot*theta1;
        semilogx(x_grid,EQE1_regre);
        theta1

        figure (3)
        semilogx(data(:,1),data(:,4),'o')
        hold on
        L0_regre = 10.^(x_plot*theta2);
        semilogx(x_grid,L0_regre);
        theta2

%% For differential analysis
        beta = 2;
        a0 = 1e-6;      %recombination zone width (10nm)
        tau = 0.6e-6;   %Lifetime of emitter [Ir(ppy)3]
        eta_PL=1;       %PL quantum yield
        eta_OC=0.2;     %outcoupling coefficient
        
        EQE_ratio = EQE0_regre./EQE1_regre;
        linearplot_x=((1+1/beta)*EQE1_regre-EQE0_regre).*(tau*L0_regre/(a0*eta_PL*eta_OC)).^(1/beta);
        linearplot_y=EQE0_regre-EQE1_regre;

        figure(4)
        semilogx(x_grid,EQE_ratio);
        figure (5)
        plot(linearplot_x,linearplot_y);


    case 'exponential'
%% For exponential regression with  h(x) = exp(theta_0+theta_1*x+theta_2*x^2 + ...) + a0
% a0 is a constant offset _*at the end of theta array*_ .
        init_theta = zeros(order+2,1);

        %fitting function h(x) = exp(theta_0+theta_1*x+t    heta_2*x^2 + ...)
        [theta,J0,exit_flag] = fminunc(@(t) (costfunc_reg(t,X,y0,lambda)),init_theta,options);
        fprintf('Final cost function is: %f\n',J0);

        %begin plotting:
        figure (1)
        semilogx(data(:,1),data(:,2),'o')
        hold on
        h = 1e-4;  %step size
        x_grid = transpose(0:h:0.25);
        x_plot = ones(length(x_grid),1);
        for k=1:order
            x_plot = [x_plot x_grid.^k];
        end
        x_plot = bsxfun(@minus,x_plot,[0 mu]);
        x_plot = bsxfun(@rdivide,x_plot,[1 sigma]);

        y_grid = exp(x_plot*theta(1:end-1))+theta(end);
        semilogx(x_grid,y_grid);
        title('Zeroth derivative','fontsize',22)
%         y_1 = diff(y_grid)/h;
%         y_2 = diff(y_1)/h;
%         y_3 = diff(y_2)/h;
%         
%         figure (2)
%         plot(x_grid(1:length(y_1)),y_1);
%         title('First derivative','fontsize',22)
%         
%         figure (3)
%         plot(x_grid(1:length(y_2)),y_2);
%         title('Second derivative','fontsize',22)
% 
%         figure (4)
%         plot(x_grid(1:length(y_3)),y_3);
%         title('Third derivative','fontsize',22)
%         
        theta

end

toc;