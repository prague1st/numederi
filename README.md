##========= What is this repo for ===========

Quite frequently scientific researchers need to take numerical derivatives on experimentally measured/computer simulated data sets. More often than not, these data sets are noisy to the zeroth order so that the conventional methods for numerical derivatives (such as 'diff' in Matlab) does not generate satisfying results. Even if they do, sometimes one may need to take the second or even higher order of numerical derivatives, and the traditional difference methods generally fail unless the original data itself is perfectly smooth with high sampling rates.

This package is targeted to solve this problem through first doing a multi-regression on the initial data, then take derivative on the fit function. The function fitting is achieved under either L2 or no regularization to tackle the problem of overfitting. 

##========= Setting up ===========

* Based on Matlab, this package should work with any versions newer than 2012b. 
* The input data is a two-column text file, corresponding to x and y values. 
* The package fits the data with a polynomial, with 'order' specifies the highest component of the fitting polynomial. 'lambda' is the regularization parameter, which controls the bias-variance trade off. 
* A detailed technical note is 'NumeDeri_Implementation_Notes.pdf'.


##========= Questions $ comments? =========

Please contact: 

* yufei@psu.edu