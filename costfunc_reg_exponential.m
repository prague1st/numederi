function [J, grad] = costfunc_reg_exponential(theta,X,y,lambda)

%costfunc_reg Compute cost and gradient for logistic regression with regularization
%   J = COSTFUNCTIONREG(theta, X, y, lambda) computes the cost of using
%   theta as the parameter for regularized logistic regression and the
%   gradient of the cost w.r.t. to the parameters. 

% Initialize some useful values
m = length(y); % number of training examples
%grad = zeros(size(theta));

%% For exponential regression with  h(x) = exp(theta_0+theta_1*x+theta_2*x^2 + ...) + a0
% a0 is a constant offset.

J = 0.5/m*(exp(X*theta(1:end-1))+theta(end)-y)'*(exp(X*theta(1:end-1))+theta(end)-y) ...
    +lambda/(2*m)*(theta'*theta-theta(1)^2);

grad = 1/m*X'*((exp(X*theta(1:end-1))+theta(end)-y).*exp(X*theta(1:end-1)));
grad(2:end) = grad(2:end)+lambda/m*theta(2:end-1);
last = 1/m*sum(exp(X*theta(1:end-1))+theta(end)-y)+lambda/m*theta(end);
grad = [grad;last];        % derivative w.r.t a0 is always zero!

end